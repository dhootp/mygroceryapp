CREATE TABLE item(
product_id VARCHAR(20) not null,
name VARCHAR(20) not null,
price int not null,
qty int DEFAULT 0,
rating float DEFAULT 0,
description VARCHAR(20) not null,
type VARCHAR(20) not null
);

CREATE TABLE cart(
cart_id VARCHAR(20) not null,
item_product_ids VARCHAR(20),
amount_payable int DEFAULT 0,
item_count int DEFAULT 0
);