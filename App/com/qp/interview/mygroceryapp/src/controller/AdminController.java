package com.qp.interview.mygroceryapp.src.controller;

import com.qp.interview.mygroceryapp.src.model.Item;
import com.qp.interview.mygroceryapp.src.services.AdminService;

import java.util.List;

public class AdminController {
    AdminService adminService = new AdminService();
    public List<Item> viewAllItems(){
        List<Item> items = adminService.viewAllItems();
        if(items.isEmpty()){
            return null;
        }
        return items;
    }

    public void addNewItem(Item item){

    }

    public void removeItem(Item item){
        if(item.getQty() == 0){
            System.out.println("Item quantity is negative");
        }

    }
    public void updateItemDetails(Item item, String productId){

    }
}
