package com.qp.interview.mygroceryapp.src.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseService {

    private static Connection con = null;

    static
    {
        final String JDBC_DRIVER = "";
        final String DB_URL = "";
        final String USER = "myGroceryApp";
        final String PASS = "password";
        try {
            Class.forName(JDBC_DRIVER);
            con = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected database successfully...");
        }
        catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
    public static Connection getConnection()
    {
        return con;
    }
}
