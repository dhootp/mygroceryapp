package com.qp.interview.mygroceryapp.src.services;

import com.qp.interview.mygroceryapp.src.model.Item;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AdminService {

    Connection conn = DatabaseService.getConnection();
    Statement stmt = null;

    public List<Item> viewAllItems(){
        ArrayList<Item> allItems = new ArrayList<>();
        try {
            stmt = conn.createStatement();
            String sql = "SELECT product_id, name, price, qty, rating, description, type from Item";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Item item = new Item(rs.getString("product_id"));
                item.setName(rs.getString("name"));
                item.setPrice(rs.getInt("price"));
                item.setQty(rs.getInt("qty"));
                item.setRating(rs.getFloat("rating"));
                item.setDescription(rs.getString("description"));
                item.setType(rs.getString("type"));
                allItems.add(item);
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return allItems;
    }
}
